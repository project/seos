(function ($, Drupal) {

  Drupal.behaviors.customBehaviour = {
    attach: function (context, settings) {
      // Your custom behaviour here.
    }
  };

  Drupal.behaviors.mobileNavigation = {
    attach: function (context, settings) {
      let main_menu = $('.main-menu nav');
      if (main_menu.length){
        main_menu.slicknav({
          prependTo: ".main-menu__mobile",
          closedSymbol: '+',
          openedSymbol: '-'
        });
      }
    }
  };

  Drupal.behaviors.headerAndToolbarHeight = {
    attach: function (context, settings) {

      function setHeaderAndToolbarHeight() {
        let headerHeight = $('.header-area').outerHeight() || 0;
        $('.dialog-off-canvas-main-canvas').css('padding-top', headerHeight);
      }

      if ($('#toolbar-administration').length) {
        setHeaderAndToolbarHeight();

        $(window).once('headerAndToolbarHeightOnce').resize(function () {
          setHeaderAndToolbarHeight();
        });

        $('#toolbar-administration').find('.toolbar-tab').find('.trigger').click(function () {
          setHeaderAndToolbarHeight();
        })
      }
    }
  };

})(jQuery, Drupal);
